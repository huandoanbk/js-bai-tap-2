/**
 * Bài 1
 */
var luongNgay;
var soNgay;
var luong;

function tinhLuong() {
  luongNgay = document.getElementById("luongMotngay").value * 1;
  soNgay = document.getElementById("soNgay").value * 1;
  luong = luongNgay * soNgay;
  // xuất kết quả
  document.getElementById("tienLuong").innerHTML =
    new Intl.NumberFormat().format(luong);
}

/**
 * Bài 2
 */

var b2Num1;
var b2Num2;
var b2Num3;
var b2Num4;
var b2Num5;
var b2Trungbinh;

function tinhTrungbinh() {
  // lấy giá trị 5 số
  b2Num1 = document.getElementById("so1").value * 1;
  b2Num2 = document.getElementById("so2").value * 1;
  b2Num3 = document.getElementById("so3").value * 1;
  b2Num4 = document.getElementById("so4").value * 1;
  b2Num5 = document.getElementById("so5").value * 1;
  // tính trung bình
  b2Trungbinh = (b2Num1 + b2Num2 + b2Num3 + b2Num4 + b2Num5) / 5;
  // xuất ra màn hình
  document.getElementById("soTrungbinh").innerHTML = b2Trungbinh;
}
/**
 * Bài 3
 */
var exRate = 23500;
var usdAmount;
var vnAmount;

function soVND() {
  // lấy số tiền USD cần đổi
  usdAmount = document.getElementById("soUSD").value * 1;
  // tính số điền VNĐ quy đổi
  vnAmount = exRate * usdAmount;
  // xuất kết quả
  document.getElementById("tienVND").innerHTML = new Intl.NumberFormat().format(
    vnAmount
  );
}

/**
 * Bài 4
 */
var b4Dai;
var b4Rong;
var b4Dientich;
var b4Chuvi;

function tinhHinhchunhat() {
  // lấy chiều dài
  b4Dai = document.getElementById("chieuDai").value * 1;
  // lấy chiều rộng
  b4Rong = document.getElementById("chieuRong").value * 1;
  // tính diện tích
  b4Dientich = b4Dai * b4Rong;
  // tính chu vi
  b4Chuvi = (b4Dai + b4Rong) * 2;
  // xuất kết quả chu vi
  document.getElementById("chuVi").innerHTML = new Intl.NumberFormat().format(
    b4Chuvi
  );
  // xuất kết quả diện tích
  document.getElementById("dienTich").innerHTML =
    new Intl.NumberFormat().format(b4Dientich);
}
/**
 * Bài 5
 */
var b5Num;
var b5Hangchuc;
var b5Hangdv;
var b5Tong;

function tinhTongkyso() {
  // lấy giá trị số có 2 chữ số
  b5Num = document.getElementById("soHaichuso").value * 1;
  // lấy ký số hàng đơn vị
  b5Hangdv = b5Num % 10;
  // lấy ký số hàng chục
  b5Hangchuc = (b5Num - b5Hangdv) / 10;
  // tính tổng 2 ký số
  b5Tong = b5Hangchuc + b5Hangdv;
  // xuất giá trị tổng 2 ký số
  document.getElementById("tongKyso").innerHTML = b5Tong;
}
